package com.example.tiki_trending_mvvm.views.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tiki_trending_mvvm.R;
import com.example.tiki_trending_mvvm.databinding.ItemProductTrendingBinding;
import com.example.tiki_trending_mvvm.models.remote.entities.BadgesNew;
import com.example.tiki_trending_mvvm.models.remote.entities.DataItem;
import com.example.tiki_trending_mvvm.views.interfaces.OnClickProductListener;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;

public class ProductTrendingAdapter extends RecyclerView.Adapter<ProductTrendingAdapter.ProductViewHolder> {
    private List<DataItem> dataItemList;

    OnClickProductListener onClickProductListener;

    public ProductTrendingAdapter(List<DataItem> dataItemList, OnClickProductListener onClickProductListener) {
        this.dataItemList = dataItemList;
        this.onClickProductListener = onClickProductListener;
    }


    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemProductTrendingBinding binding = ItemProductTrendingBinding.inflate(layoutInflater, parent, false);
        return new ProductViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        DataItem item = dataItemList.get(position);
        if (item == null) {
            return;
        }
        holder.bind(item);
        holder.binding.cProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickProductListener.onClickProductItem(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (dataItemList != null) {
            return dataItemList.size();
        }
        return 0;
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        private ItemProductTrendingBinding binding;

        public ProductViewHolder(ItemProductTrendingBinding itemProductTrendingBinding) {
            super(itemProductTrendingBinding.getRoot());
            this.binding = itemProductTrendingBinding;
        }

        public void bind(DataItem dataItem) {
            binding.setItem(dataItem);
            setBadgeItem(dataItem.getBadgesNew());
            setPrice(dataItem);
        }


        private void setBadgeItem(List<BadgesNew> badgesNew) {
            boolean isHaveBadgeTikiNow = false;
            boolean isHaveBadgePriceGuarantee = false;
            for (BadgesNew item : badgesNew) {
                if (item.getCode().equalsIgnoreCase("tikinow")) {
                    String icon = item.getIcon();
                    loadImage(binding.badge, icon);
                    isHaveBadgeTikiNow = true;
                } else if (item.getCode().equalsIgnoreCase("is_best_price_guaranteed")) {
                    String icon = item.getIcon();
                    loadImage(binding.bestPriceGuarantee, icon);
                    isHaveBadgePriceGuarantee = true;
                } else {
                    //
                }
            }
            if (!isHaveBadgePriceGuarantee) {
                binding.badge.setVisibility(View.INVISIBLE);
            }
            if (!isHaveBadgeTikiNow) {
                binding.bestPriceGuarantee.setVisibility(View.INVISIBLE);
            }
        }

        private void setPrice(DataItem item) {
            NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
            numberFormat.setMaximumFractionDigits(0);
            numberFormat.setCurrency(Currency.getInstance("VND"));
            binding.price.setText(numberFormat.format(item.getPrice()));

            int discountRate = item.getDiscountRate();
            if (discountRate > 0) {
                binding.price.setTextColor(Color.RED);
                binding.discountRate.setText("-" + discountRate + "%");
            } else {
                binding.price.setTextColor(Color.BLACK);
                binding.discountRate.setVisibility(View.GONE);
            }
        }

        private void loadImage(ImageView imageView, String url) {
            Glide.with(imageView.getContext())
                    .load(url)
                    .placeholder(R.drawable.error_image)
                    .error(R.drawable.error_image)
                    .into(imageView);
        }
    }

    @BindingAdapter("ratingValue")
    public static void setRating(RatingBar view, String rating) {
        if (view != null) {
            float rate = Float.parseFloat(rating);
            view.setRating(rate);
        }
    }

    @BindingAdapter("imageProductUrl")
    public static void loadImage(ImageView image, String url) {
        Glide.with(image.getContext())
                .load(url)
                .placeholder(R.drawable.error_image)
                .error(R.drawable.error_image)
                .into(image);
    }

}