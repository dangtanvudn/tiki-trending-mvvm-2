package com.example.tiki_trending_mvvm.views.interfaces;

import com.example.tiki_trending_mvvm.models.remote.entities.DataItem;

public interface OnClickProductListener {
    void onClickProductItem(DataItem item);
}