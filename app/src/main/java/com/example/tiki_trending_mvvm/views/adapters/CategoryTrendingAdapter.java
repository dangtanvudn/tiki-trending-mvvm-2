package com.example.tiki_trending_mvvm.views.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tiki_trending_mvvm.R;
import com.example.tiki_trending_mvvm.databinding.ItemCategoryTrendingBinding;
import com.example.tiki_trending_mvvm.models.remote.entities.Category;
import com.example.tiki_trending_mvvm.views.interfaces.OnClickCategoryListener;

import java.util.List;

public class CategoryTrendingAdapter extends RecyclerView.Adapter<CategoryTrendingAdapter.CategoryViewHolder> {
    private List<Category> categoryList;
    private OnClickCategoryListener onClickCategoryListener;

    public CategoryTrendingAdapter(List<Category> categoryList, OnClickCategoryListener onClickCategoryListener) {
        this.categoryList = categoryList;
        this.onClickCategoryListener = onClickCategoryListener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemCategoryTrendingBinding binding = ItemCategoryTrendingBinding.inflate(layoutInflater, parent, false);
        return new CategoryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category item = categoryList.get(position);
        if (item == null) {
            return;
        }
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        if (categoryList != null) {
            return categoryList.size();
        }
        return 0;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        private ItemCategoryTrendingBinding binding;

        public CategoryViewHolder(@NonNull ItemCategoryTrendingBinding itemCategoryTrendingBinding) {
            super(itemCategoryTrendingBinding.getRoot());
            binding = itemCategoryTrendingBinding;
        }

        public void bind(Category category) {
            binding.setItems(category.getItem());
            this.binding.cCategory.setOnClickListener(view -> onClickCategoryListener.invoke(category));
//            this.binding.cCategory.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    onClickCategoryListener.invoke(category);
//                }
//            });
        }

    }

    @BindingAdapter("imageCategoryUrl")
    public static void loadImage(ImageView image, String url) {
        Glide.with(image.getContext())
                .load(url)
                .placeholder(R.drawable.error_image)
                .error(R.drawable.error_image)
                .into(image);
    }


}