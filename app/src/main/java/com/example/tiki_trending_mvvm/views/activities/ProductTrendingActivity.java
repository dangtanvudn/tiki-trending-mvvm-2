package com.example.tiki_trending_mvvm.views.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.tiki_trending_mvvm.R;
import com.example.tiki_trending_mvvm.databinding.ActivityProductsTrendingBinding;
import com.example.tiki_trending_mvvm.models.remote.entities.Category;
import com.example.tiki_trending_mvvm.viewmodels.ProductTrendingViewModel;
import com.example.tiki_trending_mvvm.views.adapters.CategoryTrendingAdapter;
import com.example.tiki_trending_mvvm.views.adapters.ProductTrendingAdapter;

import java.util.List;

public class ProductTrendingActivity extends AppCompatActivity {
    private ActivityProductsTrendingBinding binding;
    private ProductTrendingViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_products_trending);
        viewModel = new ViewModelProvider(this).get(ProductTrendingViewModel.class);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
        this.getLifecycle().addObserver(viewModel);
        Log.d("Activity", "set layout");
        setCategoryTrendingAdapter();
        setProductTrendingAdapter();
    }

    private void setCategoryTrendingAdapter() {
        Log.e("setTrendingCategory", "method in");
        binding.categoryTrending.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        viewModel.getCategories().observe(this, categoryModels -> {
            CategoryTrendingAdapter adapter = new CategoryTrendingAdapter(categoryModels, item -> {
                Log.d("setTrendingCategory", item.getItem().getTitle());
                viewModel.loadProductsData(item.getItem().getCategoryId(), 0, 20);
//                Toast.makeText(ProductTrendingActivity.this, item.getItem().getTitle(),
//                        Toast.LENGTH_SHORT).show();
            });
            binding.categoryTrending.setAdapter(adapter);
        });
        Log.d("setTrendingCategory", "method out");
    }

    private void setProductTrendingAdapter() {
        Log.e("setTrendingProduct", "method in");
        binding.productTrending.setLayoutManager(new GridLayoutManager(this, 2));
        viewModel.getProducts().observe(this, productItem -> {
            ProductTrendingAdapter adapter = new ProductTrendingAdapter(productItem, item -> {
                Toast.makeText(ProductTrendingActivity.this, item.getName(),
                        Toast.LENGTH_SHORT).show();
            });
            binding.productTrending.setAdapter(adapter);
        });
        Log.d("setTrendingProduct", "method out");
    }

}

