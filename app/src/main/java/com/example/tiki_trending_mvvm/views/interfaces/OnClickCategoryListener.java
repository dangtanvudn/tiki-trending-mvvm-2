package com.example.tiki_trending_mvvm.views.interfaces;

import com.example.tiki_trending_mvvm.models.remote.entities.Category;

public interface OnClickCategoryListener {
    void invoke(Category category);
}
