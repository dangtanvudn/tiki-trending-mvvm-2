package com.example.tiki_trending_mvvm.viewmodels;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.MainThread;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ViewModel;

import com.example.tiki_trending_mvvm.models.remote.entities.Category;
import com.example.tiki_trending_mvvm.models.remote.entities.DataItem;
import com.example.tiki_trending_mvvm.models.remote.entities.Item;
import com.example.tiki_trending_mvvm.models.remote.entities.MetaData;
import com.example.tiki_trending_mvvm.models.remote.entities.ObjectTk;
import com.example.tiki_trending_mvvm.models.repositories.ProductTrendingRepository;
import com.example.tiki_trending_mvvm.views.activities.ProductTrendingActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ProductTrendingViewModel extends ViewModel implements LifecycleObserver {
    private ProductTrendingRepository productTrendingRepository = ProductTrendingRepository.getInstance();
    private MutableLiveData<MetaData> metaData = new MutableLiveData<>();
    private MutableLiveData<List<Category>> categories = new MutableLiveData<>();

    private int categoryId;
    MutableLiveData<List<DataItem>> products = new MutableLiveData<>();

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onStart() {
        Log.d("onStart", "method in");
        loadCategories(0, 20);
    }


    public void loadCategories(int cursor, int limit) {
        Log.e("loadCategories", "method in");
        productTrendingRepository.getCategories(cursor, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ObjectTk>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull ObjectTk objectTk) {
                        Log.d("loadCategories onNext", "method in");
                        Log.d("Value of ObjectTk", String.valueOf(objectTk));
                        if (objectTk == null) return;
                        metaData.setValue(objectTk.getData().getMetaData());
                        List<Category> categoryList = new ArrayList<>();
                        List<Item> items = objectTk.getData().getMetaData().getItems();
                        if (items == null || items.size() == 0) return;
                        for (int i = 0; i < items.size(); i++) {
                            categoryList.add(new Category(
                                    0, items.get(i)
                            ));
                        }
                        categories.setValue(categoryList);
                        Log.d("Value of categories", String.valueOf(categories.getValue()));
                        categoryId = items.get(0).getCategoryId();
                        loadProductsData(categoryId, 0, 20);
                        Log.d("Value of categoryId", String.valueOf(categoryId));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e("Error", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d("loadCategories", "onComplete");
                    }
                });
    }


    public void loadProductsData(int categoryId, int cursor, int limit) {
        Log.e("loadProductsData", "method in");
        productTrendingRepository.getProducts(categoryId, cursor, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ObjectTk>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull ObjectTk objectTk) {
                        Log.d("loadProductsData onNext", "method in");
                        Log.d("Value of ObjectTk", String.valueOf(objectTk));
                        if (objectTk == null) return;
                        products.setValue(objectTk.getData().getData());
                        Log.d("Value of products", String.valueOf(products.getValue()));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e("Error", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d("loadProductsData", "onComplete");
                    }
                });
    }

    public MutableLiveData<MetaData> getMetaData() {
        return metaData;
    }

    public MutableLiveData<List<Category>> getCategories() {
        Log.d("getCategories", String.valueOf(categories.getValue()));
        return categories;
    }


    public MutableLiveData<List<DataItem>> getProducts() {
        Log.d("getProducts", String.valueOf(products.getValue()));
        return products;
    }

    public static void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

}