package com.example.tiki_trending_mvvm.models.remote.entities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("meta_data")
    @Expose
    private MetaData metaData;
    @SerializedName("data")
    @Expose
    private List<DataItem> data = null;
    @SerializedName("next_page")
    @Expose
    private String nextPage;

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public List<DataItem> getData() {
        return data;
    }

    public void setData(List<DataItem> data) {
        this.data = data;
    }

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }

}