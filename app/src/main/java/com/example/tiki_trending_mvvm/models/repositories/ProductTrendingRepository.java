package com.example.tiki_trending_mvvm.models.repositories;

import android.util.Log;

import com.example.tiki_trending_mvvm.models.remote.entities.ObjectTk;
import com.example.tiki_trending_mvvm.models.remote.services.ProductTrendingApiService;
import com.example.tiki_trending_mvvm.models.remote.services.RetrofitApiService;


import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductTrendingRepository {

    private static ProductTrendingRepository instance = new ProductTrendingRepository();

    public static ProductTrendingRepository getInstance() {
        if (instance == null) {
            instance = new ProductTrendingRepository();
        }
        return instance;
    }

    private final ProductTrendingApiService productTrendingApiService;

    public ProductTrendingRepository() {
        productTrendingApiService = RetrofitApiService.getClient().create(ProductTrendingApiService.class);
    }

    public Observable<ObjectTk> getCategories(int cursor, int limit) {
        Log.e("getCategories", "method in");
        return Observable.create(emitter -> productTrendingApiService.getCategories(cursor, limit)
                .enqueue(new Callback<ObjectTk>() {
            @Override
            public void onResponse(Call<ObjectTk> call, Response<ObjectTk> response) {
                if (response.body() != null) {
                    ObjectTk responseTk = response.body();
                    Log.e("getCategories response", responseTk.toString());
                    emitter.onNext(responseTk);
                }
            }

            @Override
            public void onFailure(Call<ObjectTk> call, Throwable t) {
                Log.e("getCategories onFailure", t.getMessage());
            }
        }));
    }

    public Observable<ObjectTk> getProducts(int categoryId, int cursor, int limit) {
        Log.e("getProducts", "method in");
        return Observable.create(emitter -> productTrendingApiService.getProducts(categoryId, cursor, limit)
                .enqueue(new Callback<ObjectTk>() {
            @Override
            public void onResponse(Call<ObjectTk> call, Response<ObjectTk> response) {
                if (response.body() != null) {
                    ObjectTk responseTk = response.body();
                    Log.e("getProducts response", responseTk.toString());
                    emitter.onNext(responseTk);
                }
            }

            @Override
            public void onFailure(Call<ObjectTk> call, Throwable t) {
                Log.e("getProducts onFailure", t.getMessage());
                emitter.onError(new Exception());
            }
        }));
    }
}
