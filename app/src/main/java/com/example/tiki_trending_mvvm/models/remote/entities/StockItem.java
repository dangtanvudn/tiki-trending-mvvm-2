package com.example.tiki_trending_mvvm.models.remote.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StockItem {

    @SerializedName("max_sale_qty")
    @Expose
    private Integer maxSaleQty;
    @SerializedName("min_sale_qty")
    @Expose
    private Integer minSaleQty;
    @SerializedName("preorder_date")
    @Expose
    private Boolean preorderDate;
    @SerializedName("qty")
    @Expose
    private Integer qty;

    public Integer getMaxSaleQty() {
        return maxSaleQty;
    }

    public void setMaxSaleQty(Integer maxSaleQty) {
        this.maxSaleQty = maxSaleQty;
    }

    public Integer getMinSaleQty() {
        return minSaleQty;
    }

    public void setMinSaleQty(Integer minSaleQty) {
        this.minSaleQty = minSaleQty;
    }

    public Boolean getPreorderDate() {
        return preorderDate;
    }

    public void setPreorderDate(Boolean preorderDate) {
        this.preorderDate = preorderDate;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

}
