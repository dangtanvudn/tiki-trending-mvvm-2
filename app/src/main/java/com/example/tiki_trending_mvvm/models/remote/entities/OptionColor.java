package com.example.tiki_trending_mvvm.models.remote.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OptionColor {

    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("is_default")
    @Expose
    private Integer isDefault;
    @SerializedName("list_price")
    @Expose
    private Integer listPrice;
    @SerializedName("original_price")
    @Expose
    private Integer originalPrice;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("small_thumbnail")
    @Expose
    private String smallThumbnail;
    @SerializedName("spid")
    @Expose
    private Integer spid;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("url_path")
    @Expose
    private String urlPath;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public Integer getListPrice() {
        return listPrice;
    }

    public void setListPrice(Integer listPrice) {
        this.listPrice = listPrice;
    }

    public Integer getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getSmallThumbnail() {
        return smallThumbnail;
    }

    public void setSmallThumbnail(String smallThumbnail) {
        this.smallThumbnail = smallThumbnail;
    }

    public Integer getSpid() {
        return spid;
    }

    public void setSpid(Integer spid) {
        this.spid = spid;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

}