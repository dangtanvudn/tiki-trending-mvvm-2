package com.example.tiki_trending_mvvm.models.remote.entities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MetaData {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("more_link_text")
    @Expose
    private String moreLinkText;
    @SerializedName("more_link")
    @Expose
    private String moreLink;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("sub_title")
    @Expose
    private String subTitle;
    @SerializedName("background_image")
    @Expose
    private String backgroundImage;
    @SerializedName("title_icon")
    @Expose
    private TitleIcon titleIcon;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMoreLinkText() {
        return moreLinkText;
    }

    public void setMoreLinkText(String moreLinkText) {
        this.moreLinkText = moreLinkText;
    }

    public String getMoreLink() {
        return moreLink;
    }

    public void setMoreLink(String moreLink) {
        this.moreLink = moreLink;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public TitleIcon getTitleIcon() {
        return titleIcon;
    }

    public void setTitleIcon(TitleIcon titleIcon) {
        this.titleIcon = titleIcon;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}