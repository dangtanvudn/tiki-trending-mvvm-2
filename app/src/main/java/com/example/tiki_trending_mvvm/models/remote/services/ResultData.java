package com.example.tiki_trending_mvvm.models.remote.services;


import com.example.tiki_trending_mvvm.models.remote.entities.ObjectTk;

public interface ResultData {
    void invoke(ObjectTk objectTk);
}
