package com.example.tiki_trending_mvvm.models.remote.entities;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataItem{

    @SerializedName("badges_new")
    @Expose
    private List<BadgesNew> badgesNew = null;
    @SerializedName("brand_name")
    @Expose
    private String brandName;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("discount_rate")
    @Expose
    private Integer discountRate;
    @SerializedName("favourite_count")
    @Expose
    private Integer favouriteCount;
    @SerializedName("has_ebook")
    @Expose
    private Boolean hasEbook;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("inventory")
    @Expose
    private Inventory inventory;
    @SerializedName("inventory_status")
    @Expose
    private String inventoryStatus;
    @SerializedName("is_flower")
    @Expose
    private Boolean isFlower;
    @SerializedName("is_gift_card")
    @Expose
    private Boolean isGiftCard;
    @SerializedName("list_price")
    @Expose
    private Integer listPrice;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("order_count")
    @Expose
    private Integer orderCount;
    @SerializedName("original_price")
    @Expose
    private Integer originalPrice;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("productset_id")
    @Expose
    private Integer productsetId;
    @SerializedName("quantity_sold")
    @Expose
    private QuantitySold quantitySold;
    @SerializedName("rating_average")
    @Expose
    private Double ratingAverage;
    @SerializedName("review_count")
    @Expose
    private Integer reviewCount;
    @SerializedName("salable_type")
    @Expose
    private String salableType;
    @SerializedName("score")
    @Expose
    private Double score;
    @SerializedName("seller_product_id")
    @Expose
    private Integer sellerProductId;
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("stock_item")
    @Expose
    private StockItem stockItem;
    @SerializedName("thumbnail_height")
    @Expose
    private Integer thumbnailHeight;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;
    @SerializedName("thumbnail_width")
    @Expose
    private Integer thumbnailWidth;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("url_attendant_input_form")
    @Expose
    private String urlAttendantInputForm;
    @SerializedName("url_key")
    @Expose
    private String urlKey;
    @SerializedName("url_path")
    @Expose
    private String urlPath;
    @SerializedName("badges")
    @Expose
    private List<Badge> badges = null;
    @SerializedName("installment_info")
    @Expose
    private InstallmentInfo installmentInfo;
    @SerializedName("option_color")
    @Expose
    private List<OptionColor> optionColor = null;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;
    @SerializedName("bundle_deal")
    @Expose
    private Boolean bundleDeal;

    public List<BadgesNew> getBadgesNew() {
        return badgesNew;
    }

    public void setBadgesNew(List<BadgesNew> badgesNew) {
        this.badgesNew = badgesNew;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(Integer discountRate) {
        this.discountRate = discountRate;
    }

    public Integer getFavouriteCount() {
        return favouriteCount;
    }

    public void setFavouriteCount(Integer favouriteCount) {
        this.favouriteCount = favouriteCount;
    }

    public Boolean getHasEbook() {
        return hasEbook;
    }

    public void setHasEbook(Boolean hasEbook) {
        this.hasEbook = hasEbook;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public String getInventoryStatus() {
        return inventoryStatus;
    }

    public void setInventoryStatus(String inventoryStatus) {
        this.inventoryStatus = inventoryStatus;
    }

    public Boolean getIsFlower() {
        return isFlower;
    }

    public void setIsFlower(Boolean isFlower) {
        this.isFlower = isFlower;
    }

    public Boolean getIsGiftCard() {
        return isGiftCard;
    }

    public void setIsGiftCard(Boolean isGiftCard) {
        this.isGiftCard = isGiftCard;
    }

    public Integer getListPrice() {
        return listPrice;
    }

    public void setListPrice(Integer listPrice) {
        this.listPrice = listPrice;
    }

    public String getName() {
        return name;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getProductsetId() {
        return productsetId;
    }

    public void setProductsetId(Integer productsetId) {
        this.productsetId = productsetId;
    }

    public QuantitySold getQuantitySold() {
        return quantitySold;
    }

    public Double getRatingAverage() {
        return ratingAverage;
    }

    public Integer getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getSalableType() {
        return salableType;
    }

    public void setSalableType(String salableType) {
        this.salableType = salableType;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Integer getSellerProductId() {
        return sellerProductId;
    }

    public void setSellerProductId(Integer sellerProductId) {
        this.sellerProductId = sellerProductId;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public StockItem getStockItem() {
        return stockItem;
    }

    public void setStockItem(StockItem stockItem) {
        this.stockItem = stockItem;
    }

    public Integer getThumbnailHeight() {
        return thumbnailHeight;
    }

    public void setThumbnailHeight(Integer thumbnailHeight) {
        this.thumbnailHeight = thumbnailHeight;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public Integer getThumbnailWidth() {
        return thumbnailWidth;
    }

    public void setThumbnailWidth(Integer thumbnailWidth) {
        this.thumbnailWidth = thumbnailWidth;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrlAttendantInputForm() {
        return urlAttendantInputForm;
    }

    public void setUrlAttendantInputForm(String urlAttendantInputForm) {
        this.urlAttendantInputForm = urlAttendantInputForm;
    }

    public String getUrlKey() {
        return urlKey;
    }

    public void setUrlKey(String urlKey) {
        this.urlKey = urlKey;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    public List<Badge> getBadges() {
        return badges;
    }

    public void setBadges(List<Badge> badges) {
        this.badges = badges;
    }

    public InstallmentInfo getInstallmentInfo() {
        return installmentInfo;
    }

    public void setInstallmentInfo(InstallmentInfo installmentInfo) {
        this.installmentInfo = installmentInfo;
    }

    public List<OptionColor> getOptionColor() {
        return optionColor;
    }

    public void setOptionColor(List<OptionColor> optionColor) {
        this.optionColor = optionColor;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Boolean getBundleDeal() {
        return bundleDeal;
    }

    public void setBundleDeal(Boolean bundleDeal) {
        this.bundleDeal = bundleDeal;
    }

}