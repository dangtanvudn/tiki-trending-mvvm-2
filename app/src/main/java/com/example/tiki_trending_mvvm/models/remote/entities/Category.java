package com.example.tiki_trending_mvvm.models.remote.entities;

import com.example.tiki_trending_mvvm.models.remote.entities.Item;

public class Category {
    private int cursor = 0;
    private Item item;

    public Category(int cursor, Item item) {
        this.cursor = cursor;
        this.item = item;
    }

    public int getCursor() {
        return cursor;
    }

    public void setCursor(int cursor) {
        this.cursor = cursor;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
