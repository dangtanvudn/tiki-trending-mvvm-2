package com.example.tiki_trending_mvvm.models.remote.services;

import com.example.tiki_trending_mvvm.models.remote.entities.MetaData;
import com.example.tiki_trending_mvvm.models.remote.entities.ObjectTk;


import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ProductTrendingApiService {

    @GET("shopping-trend/api/trendings/hub?")
    Call<ObjectTk> getCategories(
            @Query("cursor") int cursor,
            @Query("limit") int limit
    );

    @GET("shopping-trend/api/trendings/hub/category_id/{id}?")
    Call<ObjectTk> getProducts(
            @Path("id") int id,
            @Query("cursor") int cursor,
            @Query("limit") int limit
    );
}
