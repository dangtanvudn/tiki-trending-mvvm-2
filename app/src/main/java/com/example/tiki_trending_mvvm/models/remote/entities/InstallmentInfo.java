package com.example.tiki_trending_mvvm.models.remote.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstallmentInfo {

    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("redirect_url")
    @Expose
    private String redirectUrl;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

}
